﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;

    private float rotation = 120;

    void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        if (anotherCollider.name == "Player1" || anotherCollider.name == "Player2")
        {
            gameManager.GameOver(anotherCollider.name);
            GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        }
    }

    public void SetRotation(float rot)
    {
        rotation = rot;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate (0,0,rotation*Time.deltaTime);
    }
}

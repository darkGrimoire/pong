﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chili : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D anotherCollider)
    {
        if (anotherCollider.name == "Player1" || anotherCollider.name == "Player2")
        {
            anotherCollider.gameObject.SendMessage("PowerUp", null, SendMessageOptions.RequireReceiver);
            Destroy(gameObject);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

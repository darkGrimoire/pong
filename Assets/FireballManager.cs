﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballManager : MonoBehaviour
{
    public GameObject fireball;
    public float xInitialForce;
    public float yInitialForce;

    private bool generation = true;
    private float spawnTime = 5.0f;
    IEnumerator Spawn()
    {
        float yCoordinate = 11;
        float xCoordinate = Random.Range(-15, 15);
        float yRandomInitialForce = Random.Range(-yInitialForce, 0);
        float xRandomInitialForce = Random.Range(-xInitialForce, xInitialForce);
        GameObject spawnedFireball = Instantiate(fireball, new Vector2(xCoordinate, yCoordinate), Quaternion.identity);
        spawnedFireball.SetActive(true);
        spawnedFireball.name = "Fireball_Spawn";
        spawnedFireball.GetComponent<Fireball>().SetRotation(yRandomInitialForce * 100);
        spawnedFireball.GetComponent<Rigidbody2D>().AddForce(new Vector2(xRandomInitialForce, yRandomInitialForce));
        yield return new WaitForSeconds(Random.Range(spawnTime, spawnTime*2));
        if (generation)
        {
            if (spawnTime > 0.5f)
            {
                spawnTime *= 0.8f;
            }
            StartCoroutine(Spawn());
        }
    }
    public IEnumerator StartSpawn()
    {
        yield return new WaitForSeconds(8);
        generation = true;
        StartCoroutine(Spawn());
    }

    public void StopSpawn()
    {
        generation = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

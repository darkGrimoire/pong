﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpControl : MonoBehaviour
{
    public GameObject chili;

    private bool generation;
    IEnumerator Spawn()
    {
        float choosenPlayer = Mathf.Floor(Random.Range(1, 3));
        float yCoordinate = Random.Range(-9, 9);
        float xCoordinate;
        if (choosenPlayer == 1)
        {
            xCoordinate = -15;
        }
        else
        {
            xCoordinate = 15;
        }
        GameObject spawnedChili = Instantiate(chili, new Vector2(xCoordinate, yCoordinate), Quaternion.identity);
        spawnedChili.SetActive(true);
        spawnedChili.name = "Chili_Spawn";
        yield return new WaitForSeconds(Random.Range(4, 6));
        if (generation)
        {
            StartCoroutine(Spawn());
        }
    }

    public IEnumerator StartSpawn()
    {
        yield return new WaitForSeconds(5);
        generation = true;
        StartCoroutine(Spawn());
    }

    public void StopSpawn()
    {
        generation = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartSpawn());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
